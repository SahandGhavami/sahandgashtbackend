<?php

function checkInput($params)
{
    $available = true;
    $missingParams = [];
    foreach ($params as $param) {
        if (!input($param)) {
            $available = false;
            $missingParams[] = $param;
        }
    }

    if (!$available) {
        respond(
            Error('These params are required and not available: ' . implode(', ', $missingParams) . '.')
        );
        die();
    }
}

function input(string $name, $default = null)
{
    return $_GET[$name] ?? $_POST[$name] ?? $default;
}

function getTokenFromHeader()
{
    $headers = getallheaders();

    if ($headers) {
        if (isset($headers['Authorization'])) {
            return explode(' ', $headers['Authorization'])[1];
        }
    }

    return null;
}

function getLoggedInUser(\Medoo\Medoo $database)
{
    $userId = checkToken(getTokenFromHeader());

    if ($userId === null) {
        return null;
    }

    $result = $database->select("users", '*', [
        "user_id" => $userId,
    ]);

    if (count($result) > 0) {
        return $result[0];
    }

    return null;
}

function respond($response)
{
    header('Content-Type: application/json');
    echo json_encode($response);
}

function generateToken($userId): string
{
    $claim = [
        'user_id' => $userId,
    ];

    return \Firebase\JWT\JWT::encode($claim, JWT_SECRET);
}

function checkToken(string $token)
{
    try {
        $decoded = \Firebase\JWT\JWT::decode($token, JWT_SECRET, ['HS256']);
        return $decoded->user_id;
    } catch (Throwable $t) {
        return null;
    }
}

function hashPassword(string $string)
{
    return sha1(md5($string) . "sahand");
}

function enrichTour($tour, \Medoo\Medoo $database): array
{
    if (!isset($tour) || !$tour) return [];

    $totalReservedCount = $database->count("reservations", [
        "tour_id" => input('id'),
    ]);

    $tour['remaining_capacity'] = $tour['capacity'] - $totalReservedCount;

    $tour['has_user_reserved_before'] = false;

    if ($user = getLoggedInUser($database)) {
        $userReservedCount = $database->count("reservations", [
            "tour_id" => input('id'),
            "user_id" => $user['user_id'],
        ]);
        $tour['has_user_reserved_before'] = $userReservedCount > 0;
    }

    if (isset($tour['images'])) {
        if (preg_match('/,/', $tour['images'])) {
            $tour['images'] = explode(',', $tour['images']);
            for ($i = 0; $i < count($tour['images']); $i++) {
                $actualUrl = $tour['images'][$i];
                if (substr($actualUrl, 0, 4) != "http") {
                    $tour['images'][$i] = WEBSITE_ADDRESS . "/" . $tour['images'][$i];
                }
            }
        } else {
            $tour['images'] = [WEBSITE_ADDRESS . "/" . $tour['images']];
        }
    }

    return $tour;
}
