<?php

require_once 'Constants.php';
require_once 'Medoo.php';

use Medoo\Medoo;

return new Medoo([
    'database_type' => 'mysql',
    'database_name' => DB_NAME,
    'server' => DB_HOST,
    'username' => DB_USER,
    'password' => DB_PASS,
    'charset' => 'utf8',
    'collation' => 'utf8_persian_ci'
]);
