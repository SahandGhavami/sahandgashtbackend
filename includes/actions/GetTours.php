<?php

function GetTours(\Medoo\Medoo $database): array
{
    $now = date("Y-m-d H:i:s");

    return [
        'error' => false,
        'message' => 'Request successfully completed!',
        'tour' => enrichTour(
            $database
                ->query("SELECT tours.*,GROUP_CONCAT(ti.image_url) images FROM tours INNER JOIN tour_images ti ON ti.tour_id = tours.id WHERE tours.date > '" . $now . "' GROUP BY tours.id")
                ->fetch(PDO::FETCH_ASSOC),
            $database
        )
    ];
}