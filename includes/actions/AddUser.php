<?php

function AddUser(\Medoo\Medoo $database): array
{
    $response = [];

    checkInput(['user_name', 'user_password', 'user_email', 'user_lname', 'user_phone_number']);

    /*$emailThatSignedBefore = $database->select("users","user_email");
    $emailThatSignedBeforeLength = count($emailThatSignedBefore);
    for ($x = 0 ; $x <= $emailThatSignedBeforeLength ; $x++) {
        if (input('user_email') == $emailThatSignedBefore[$x]) {
            return Error('This email exist!!');
        }
    }*/

    $result = $database->insert("users", [
        "user_name" => input('user_name'),
        "user_password" => hashPassword(input('user_password')),
        "user_email" => input('user_email'),
        "user_lname" => input("user_lname"),
        "user_phone_number" => input("user_phone_number")
    ]);

    if ($result->rowCount() > 0) {
        $response['error'] = false;
        $response['message'] = "User added successfully!";
        $response['token'] = generateToken($database->id());
    } else {
        return Error('User registration has been failed!');
    }

    return $response;
}
