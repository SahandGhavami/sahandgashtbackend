<?php
function GetPassengers(\Medoo\Medoo $database): array
{
    $userId = checkToken(getTokenFromHeader());

    if ($userId === null) {
        return Error("user has not been found!!");
    }

    return [
        'error' => false,
        'message' => 'Request successfully completed!',
        'Users' =>
            $database
                ->query("SELECT reservations.user_id, users.user_name, users.user_phone_number, users.user_lname FROM reservations 
                               INNER JOIN users ON users.user_id = reservations.user_id 
                               WHERE reservations.tour_id = ". input("id"))
                ->fetchAll(PDO::FETCH_ASSOC)
    ];
}