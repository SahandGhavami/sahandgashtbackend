<?php

function GetTour(\Medoo\Medoo $database): array
{
    $now = date("Y-m-d H:i:s");

    $tours = $database
        ->query("SELECT tours.*,GROUP_CONCAT(ti.image_url) images FROM tours INNER JOIN tour_images ti ON ti.tour_id = tours.id where tours.date > '" . $now . "' GROUP BY tours.id")
        ->fetchAll(PDO::FETCH_ASSOC);

    for ($i = 0; $i < count($tours); $i++) {
        $tours[$i] = enrichTour($tours[$i], $database);
    }

    return [
        'error' => false,
        'message' => 'Request successfully completed!',
        'tours' => $tours,
    ];
}