<?php

function Reservation(\Medoo\Medoo $database): array
{
    $response = [];
    $userId = checkToken(getTokenFromHeader());

    if ($userId === null) {
        return error("user has not been found!");
    }

    $tour = $database->select("tours", '*', ['id' => input('tour_id')])[0];

    $totalReservedCount = $database->count("reservations", [
        "tour_id" => input('tour_id'),
    ]);

    /*$database->update("tours" ,[
        "reserved_number"  =>  $totalReservedCount
    ],[
        'id' => input('tour_id')
    ]);*/

    $tour['remaining_capacity'] = $tour['capacity'] - $totalReservedCount;

    if ($tour['remaining_capacity'] < 1) {
        return Error("No more capacity is left");
    }

    $result = $database->insert("reservations", [
        "tour_id" => input('tour_id'),
        "user_id" => $userId,
        "reserved_at"=> date('Y-m-d H:i:s')
    ]);

    if ($result) {
        $response['error'] = false;
        $response['message'] = "Reserverd successfully!";
    } else {
        return Error('Reservation has been failed!');
    }

    return $response;
}
