<?php

function Me(\Medoo\Medoo $database): array
{
    $response = [];

    $user = getLoggedInUser($database);

    if ($user === null) {
        return Error('Invalid token.');
    }

    $response['error'] = false;
    $response['user'] = $user;

    return $response;
}
