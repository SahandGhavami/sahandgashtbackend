<?php

function UpdateTour(\Medoo\Medoo $database): array
{
    $response = [];

    checkInput(array('name', 'cost', 'date', 'image', 'description', 'details', 'capacity', 'reserved_number', 'location', 'category', 'id'));
    //$db=new DbOperation();
    $result = $database->update("tours", [
        "name" => $_POST['name'],
        "cost" => $_POST['cost'],
        "date" => $_POST['date'],
        "image" => $_POST['image'],
        "description" => $_POST['description'],
        "details" => $_POST['details'],
        "capacity" => $_POST['capacity'],
        "reserved_number" => $_POST['reserved_number'],
        "location" => $_POST['location'],
        "category" => $_POST['category']
    ], [
        "id" => $_POST['id']
    ]);

    if ($result) {
        $response['error'] = false;
        $response['message'] = "Tour updates successfully!";
        $response['Tours'] = $database->select("tour", "*");
    } else {
        $response['error'] = true;
        $response['message'] = 'Some error occured.try again!';
    }

    return $response;
}
