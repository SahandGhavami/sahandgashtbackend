<?php

function MyTours(\Medoo\Medoo $database): array
{
    $now = date("Y-m-d H:i:s");

    $userId = checkToken(getTokenFromHeader());

    if ($userId === null) {
        return Error("user has not been found!!");
    }

    $tours = $database
        ->query("SELECT tours.* ,reservations.reserved_at ,GROUP_CONCAT(tour_images.image_url) images FROM reservations INNER JOIN tours ON tours.id = reservations.tour_id INNER JOIN tour_images ON tour_images.tour_id = reservations.tour_id WHERE reservations.user_id = $userId AND tours.date > '" . $now . "' GROUP BY tours.id")
        ->fetchAll(PDO::FETCH_ASSOC);

    for ($i = 0; $i < count($tours); $i++) {
        $tours[$i] = enrichTour($tours[$i], $database);
    }

    return [
        'error' => false,
        'message' => 'Request successfully completed!',
        'tours' => $tours,
    ];

}
