<?php

function WhoAmI(\Medoo\Medoo $database): array
{
    $response = [];

    checkInput(['token']);

    $userId = checkToken(input('token'));

    if ($userId === null) {
        return Error('Invalid token.');
    }

    $result = $database->select("users", '*', [
        "user_id" => $userId,
    ]);

    $response['error'] = false;
    $response['user'] = $result[0];

    return $response;
}
