<?php

function Login(\Medoo\Medoo $database): array
{
    $response = [];

    checkInput(['user_email', 'user_password']);

    $result = $database->select("users", '*', [
        "user_password" => hashPassword(input('user_password')),
        "user_email" => input('user_email')
    ]);

    if (count($result) > 0) {
        $response['error'] = false;
        $response['message'] = "Logged in successfully!";
        $response['token'] = generateToken($result[0]['user_id']);
    } else {
        $response['error'] = true;
        $response['message'] = 'User not found.';
    }

    return $response;
}
