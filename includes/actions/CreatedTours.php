<?php

function CreatedTours(\Medoo\Medoo $database): array
{
    $userId = checkToken(getTokenFromHeader());

    if ($userId === null) {
        return Error("user has not been found!!");
    }

    $toursQuery = $database
        ->query("SELECT tours.* FROM tours WHERE tour_owner_id = $userId GROUP BY tours.id")
        ->fetchAll(PDO::FETCH_ASSOC);

    $tours = [];
    for ($i = 0; $i < count($toursQuery); $i++) {
        $tours[] = enrichTour($toursQuery[$i], $database);
    }

    return [
        'error' => false,
        'message' => 'Request successfully completed!',
        'tours' => $tours,
    ];
}