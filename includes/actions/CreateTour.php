<?php
function CreateTour(\Medoo\Medoo $database): array
{
    $response = [];

    $userId = checkToken(getTokenFromHeader());

    file_put_contents("log.txt", json_encode([
        'files' => $_FILES,
        'post' => $_POST
    ], JSON_PRETTY_PRINT));

    checkInput(['name', 'cost', 'date', 'return_date', 'description', 'details', 'capacity', 'location']);

    $database->insert("tours", [
        "name" => input('name'),
        "cost" => input('cost'),
        "date" => input('date'),
        "return_date" => input('return_date'),
        "description" => input('description'),
        "details" => input('details'),
        "capacity" => input('capacity'),
        "location" => input('location'),
        "tour_owner_id" => $userId
    ]);
    $tourId = $database->id();

    $error = uploadImages($database, $tourId);
    if (isset($error)) {
        $database->delete('tours', ['id' => $tourId]);
        return [
            'error' => true,
            'message' => $error,
            'tour' => null,
        ];
    }

    return [
        'error' => false,
        'message' => 'Request successfully completed!',
        'tour_id' => $tourId,
    ];
}

function uploadImages(\Medoo\Medoo $database, $tourId)
{
    if (!isset($_FILES["tour_image"])) {
        return "تصویری برای آپلود انتخاب نشده است.";
    }

    $files = $_FILES["tour_image"];
    for ($i = 0; $i < count($files['name']); $i++) {
        $file = [
            'tmp_name' => $files['tmp_name'][$i],
            'name' => $files['name'][$i],
        ];

        $filePath = "images/" . basename($file["name"]);

        if (getimagesize($file["tmp_name"]) && move_uploaded_file($file["tmp_name"], "../" . $filePath)) {
            $database->insert("tour_images", [
                "image_url" => $filePath,
                "tour_id" => $tourId,
            ]);
        } else {
            return "Some error happened.";
        }
    }

    return null;
}