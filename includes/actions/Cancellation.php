<?php
function Cancellation(\Medoo\Medoo $database): array
{

    $userId = checkToken(getTokenFromHeader());

    $database -> query("DELETE FROM reservations WHERE reservations.tour_id = " . input('id') . " AND reservations.user_id = $userId ");

    return [
        'error' => false,
        'message' => 'Request successfully completed!',
    ];
}