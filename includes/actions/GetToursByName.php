<?php

function GetToursByName(\Medoo\Medoo $database): array
{
    $now = date("Y-m-d H:i:s");

    $toursQuery = $database
        ->query("SELECT tours.*,GROUP_CONCAT(ti.image_url) images FROM tours INNER JOIN tour_images ti ON ti.tour_id = tours.id WHERE tours.name like '%".input('name')."%' AND tours.date > '" . $now . "' GROUP BY tours.id")
        ->fetchAll(PDO::FETCH_ASSOC);

    $tours = [];
    for ($i = 0; $i < count($toursQuery); $i++) {
        $tours[] = enrichTour($toursQuery[$i], $database);
    }

    return [
        'error' => false,
        'message' => 'Request successfully completed!',
        'tours' => $tours,
    ];
}



