<?php

function Error(string $message): array
{
    return [
        'error' => true,
        'message' => $message,
    ];
}
