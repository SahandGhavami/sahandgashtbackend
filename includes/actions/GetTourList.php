<?php

function GetTourList(\Medoo\Medoo $database): array
{
    $now = date("Y-m-d H:i:s");

    return [
        'error' => false,
        'message' => 'Request successfully completed!',
        'tours' => [
            [
                'name' => 'یه سری تور',
                'color' => '#ffffff',
                'tours' => getEnrichedTourByQuery($database, "SELECT tours.*,GROUP_CONCAT(ti.image_url) images FROM tours INNER JOIN tour_images ti ON ti.tour_id = tours.id where tours.date > '" . $now . "' GROUP BY tours.id")
            ],
            [
                'name' => 'یه سری تور دیگه',
                'color' => '#046A7D',
                'tours' => getEnrichedTourByQuery($database, "SELECT tours.*,GROUP_CONCAT(ti.image_url) images FROM tours INNER JOIN tour_images ti ON ti.tour_id = tours.id where tours.date > '" . $now . "' GROUP BY tours.id")
            ],
            [
                'name' => 'یه سری تور دیگه ی جدید!',
                'color' => '#ffffff',
                'tours' => getEnrichedTourByQuery($database, "SELECT tours.*,GROUP_CONCAT(ti.image_url) images FROM tours INNER JOIN tour_images ti ON ti.tour_id = tours.id where tours.date > '" . $now . "' GROUP BY tours.id")
            ]
        ],
    ];
}

function getEnrichedTourByQuery(\Medoo\Medoo $database, $query)
{
    $tours = $database
        ->query($query)
        ->fetchAll(PDO::FETCH_ASSOC);

    for ($i = 0; $i < count($tours); $i++) {
        $tours[$i] = enrichTour($tours[$i], $database);
    }

    return $tours;
}