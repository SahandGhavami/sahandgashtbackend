<?php

require_once '../bootstrap.php';

$response = [];

switch (input('apicall')) {
    default:
        $response = Error('Invalid API Call');
        break;

    case 'createtour':
        $response = CreateTour($database);
        break;

    case 'reservation':
        $response = Reservation($database);
        break;

    case 'mytours':
        $response = MyTours($database);
        break;

    case 'gettoursbyname':
        $response = GetToursByName($database);
        break;

    case 'createdtours':
        $response = CreatedTours($database);
        break;

    case 'gettours':
        $response = GetTours($database);
        break;

    case 'getpassengers':
        $response = GetPassengers($database);
        break;

    case 'cancellation':
        $response = Cancellation($database);
        break;

    case 'gettourlist':
        $response = GetTourList($database);
        break;

    case 'gettourbyid':
        $response = GetTourById($database);
        break;

    case 'login':
        $response = Login($database);
        break;

    case 'whoami':
        $response = WhoAmI($database);
        break;

    case 'me':
        $response = Me($database);
        break;

    case 'gettour':
        $response = GetTour($database);
        break;

    case 'updatetour':
        $response = UpdateTour($database);
        break;

    case 'adduser':
        $response = AddUser($database);
        break;
}

respond($response);
