<?php

require_once __DIR__ . '/vendor/autoload.php';

require_once __DIR__ . '/includes/Helpers.php';

require_once __DIR__ . '/includes/actions/AddUser.php';
require_once __DIR__ . '/includes/actions/Login.php';
require_once __DIR__ . '/includes/actions/WhoAmI.php';
require_once __DIR__ . '/includes/actions/Reservation.php';
require_once __DIR__ . '/includes/actions/MyTours.php';
require_once __DIR__ . '/includes/actions/GetTours.php';
require_once __DIR__ . '/includes/actions/GetToursByName.php';
require_once __DIR__ . '/includes/actions/CreatedTours.php';
require_once __DIR__ . '/includes/actions/Me.php';
require_once __DIR__ . '/includes/actions/CreateTour.php';
require_once __DIR__ . '/includes/actions/GetPassengers.php';
require_once __DIR__ . '/includes/actions/Cancellation.php';
require_once __DIR__ . '/includes/actions/GetTourList.php';
require_once __DIR__ . '/includes/actions/GetTour.php';
require_once __DIR__ . '/includes/actions/GetTourById.php';
require_once __DIR__ . '/includes/actions/UpdateTour.php';
require_once __DIR__ . '/includes/actions/Error.php';

/** @var \Medoo\Medoo $database */
$database = require_once __DIR__ . '/includes/Database.php';

date_default_timezone_set('Asia/Tehran');

@mkdir("../images");